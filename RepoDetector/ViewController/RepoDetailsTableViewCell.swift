//
//  RepoDetailsTableViewCell.swift
//  RepoDetector
//
//  Created by Ehab on 11/30/17.
//  Copyright © 2017 Github. All rights reserved.
//

import Foundation
import PINRemoteImage

class RepoDetailsTableViewCell: UITableViewCell {
    @IBOutlet weak var subscriberImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    /*
     setup current cell
     @param repo: Repository
     */
    func setup(subscriber: Subscriber) {
        self.nameLabel?.text = subscriber.name ?? ""
        self.subscriberImageView?.setImageWithURL(urlString: subscriber.avatarUrl)
    }
}
