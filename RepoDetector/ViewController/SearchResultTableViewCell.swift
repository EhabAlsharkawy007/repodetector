//
//  SearchResultTableViewCell.swift
//  RepoDetector
//
//  Created by Ehab on 11/30/17.
//  Copyright © 2017 Github. All rights reserved.
//

import Foundation
import PINRemoteImage

class SearchResultTableViewCell: UITableViewCell {
    @IBOutlet weak var ownerImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var forkNumberLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    /*
     setup current cell
     @param repo: Repository
     */
    func setup(repo: Repository) {
        self.ownerImageView?.setImageWithURL(urlString: repo.owner?.avatarUrl)
        
        self.titleLabel.text = repo.name?.count == 0 ? LocalizationConstants.noRepoTitle.localized: repo.name
        if let forks : Int = repo.forks {
            self.forkNumberLabel.text = String(forks)
        } else{
            self.forkNumberLabel.text = LocalizationConstants.noRepoForks.localized
        }
        if let overview: String = repo.description,overview.count > 0  {
            self.overviewLabel.text = overview
        } else {
            self.overviewLabel.text = LocalizationConstants.noRepoOverview.localized
            
        }
    }
}
