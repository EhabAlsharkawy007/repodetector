//
//  LocalizationConstants.swift
//  RepoDetector
//
//  Created by Ehab on 11/30/17.
//  Copyright © 2017 Github. All rights reserved.
//

import Foundation
//MARK: - Localization
enum LocalizationConstants : String{
    case title = "Search_Title"
    case errorViewTitle = "Error_View_Title"
    case noRepoErrorViewBody = "No_Repo_Error_View_Body"
    case noRepoErrorViewOk = "No_Repo_Error_View_Ok"
    case noRepoTitle = "No_Repo_title"
    case noRepoForks = "No_Repo_Forks"
    case noRepoOverview = "No_Repo_Overview"
    case noSubscribers = "No_Subscribers"

    // get value of self from localizable file
    var localized : String {
        return self.rawValue.localized
    }
}
