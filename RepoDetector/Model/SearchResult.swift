//
//  SearchResult.swift
//  RepoDetector
//
//  Created by Ehab on 11/29/17.
//  Copyright © 2017 Github. All rights reserved.
//

import Foundation

class SearchResult : BaseModel {
    var totalCount: Int?
    var incompleteResults: Bool?
    var items: [Repository] = [Repository]()
}
