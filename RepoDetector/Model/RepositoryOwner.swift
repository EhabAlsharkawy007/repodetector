//
//  RepositoryOwner.swift
//  RepoDetector
//
//  Created by Ehab on 11/29/17.
//  Copyright © 2017 Github. All rights reserved.
//

import Foundation

class RepositoryOwner : BaseModel {
    var id: Int?
    var avatarUrl: String?
}

