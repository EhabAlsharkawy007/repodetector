//
//  RepositoryOwner+SwiftyJSON.swift
//  RepoDetector
//
//  Created by Ehab on 11/29/17.
//  Copyright © 2017 Github. All rights reserved.
//

import Foundation
import SwiftyJSON

/*
 Extension for RepositoryOwner + SwiftyJSON, is responsable for parsing using json
 */
extension RepositoryOwner {
    func parse(json: JSON) -> BaseModel {
        self.id = json[RepositoryOwnerModelConstants.id.rawValue].int
        self.avatarUrl = json[RepositoryOwnerModelConstants.avatarUrl.rawValue].string
        return self
    }
}
