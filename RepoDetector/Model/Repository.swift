//
//  repository.swift
//  RepoDetector
//
//  Created by Ehab on 11/29/17.
//  Copyright © 2017 Github. All rights reserved.
//

import Foundation

class Repository : BaseModel {
    var id: Int?
    var name : String?
    var description: String?
    var forks: Int?
    var owner: RepositoryOwner?
    var subscribersURL : String?
}
