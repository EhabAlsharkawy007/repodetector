//
//  Subscriber.swift
//  RepoDetector
//
//  Created by Ehab on 11/30/17.
//  Copyright © 2017 Github. All rights reserved.
//

import Foundation

class Subscriber : BaseModel {
    var id: Int?
    var avatarUrl: String?
    var name: String?
}
