//
//  StateManager.swift
//  RepoDetector
//
//  Created by Ehab on 11/27/17.
//  Copyright © 2017 Github. All rights reserved.
//

import Foundation

enum ServiceEnvironment : String {
    case production // default Environment
    
    var serviceConfigration : ServiceConfigration {
        switch self {
        case .production:
            return ServiceConfigration(baseURL: ProductionConfigrations.baseURL.rawValue)
        }
    }
}

/* StateManager is responsible for App Environment*/
struct StateManager {
    
    private var appServiceEnvironmentKey : String {
        get{
            return "AppServiceEnvironment"
        }
    }
    
    /*
     serviceConfigration of type ServiceConfigration has value of current Service Configration
     */
    var serviceConfigration: ServiceConfigration {
        get{
            if let appServiceEnvironment : ServiceEnvironment =  self.getCurrentEnvironment() {
                return appServiceEnvironment.serviceConfigration
            }
            return ServiceEnvironment.production.serviceConfigration
        }
    }
    
    func set(environment: ServiceEnvironment) {
        UserDefaults.standard.setValue(environment.rawValue, forKey: self.appServiceEnvironmentKey)
    }
    
    func getCurrentEnvironment() -> ServiceEnvironment?{
        if let appEnvironment : String = UserDefaults.standard.value(forKey: self.appServiceEnvironmentKey) as? String, let appServiceEnvironment : ServiceEnvironment = ServiceEnvironment(rawValue: appEnvironment) {
            return appServiceEnvironment
        }
        return nil
    }
}
