//
//  ServiceConfigration.swift
//  RepoDetector
//
//  Created by Ehab on 11/27/17.
//  Copyright © 2017 Github. All rights reserved.
//

import Foundation

/*
 Production Configrations of servise domain
 */
enum ProductionConfigrations : String {
    case baseURL = "https://api.github.com"
}

/* Service Configration is responsable for Service Domain Configration*/
struct ServiceConfigration {
    var baseURL: String
    
    /*
     Intialization
     @param baseURL String like "https://api.github.com/"
     */
    init(baseURL: String) {
        self.baseURL = baseURL
    }
}
