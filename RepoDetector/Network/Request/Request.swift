//
//  Request.swift
//  RepoDetector
//
//  Created by Ehab on 11/27/17.
//  Copyright © 2017 Github. All rights reserved.
//

import Foundation

protocol Request {
    var request : URLRequest { get }
}
