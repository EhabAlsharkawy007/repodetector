//
//  NSError+Network.swift
//  RepoDetector
//
//  Created by Ehab on 11/27/17.
//  Copyright © 2017 Github. All rights reserved.
//

import Foundation

struct NetworkErrorConstants {
    struct Description {
        static let timeOut: String = "Time out"
        static let cancelled: String = "Cancelled"
        static let noConnectionDevice: String = "No connection device"
        static let noConnectionToHost: String = "No connection to host"
        static let serverNotFound: String = "Server not found"
        static let connectionLost: String = "Connection lost"
        static let emptyResponse: String = "Empty response"
        static let parser: String = "Parser error"
        static let invalidURL: String = "Invalid URL"
        
    }
    struct Key {
        static let client: String = "ERR_HTTPS_4xx"
        static let timeout: String = "ERR_TIMEOUT"
        static let cancelled: String = "ERR_CANCELLED"
        static let noConnectionDevice: String = "ERR_NOCONNECTIONDEVICE"
        static let noConnectionToHost: String = "ERR_NOCONNECTIONTOHOST"
        static let serverNotFound: String = "ERR_SERVERNOTFOUND"
        static let connectionLost: String = "ERR_CONNECTION_LOST"
        static let emptyResponse: String = "ERR_EMPTYRESPONSE"
        static let parser: String = "PARSER_ERROR"
        static let invalidURL: String = "Invalid URL"
    }
}

enum NetworkErrorCode: Int {
    case noConnectionDevice = -1004
    case noConnectionToHost = -1009
    case connectionLost = -1005
    case encoding = -1016
    case timeOut = -1001
    case serverNotFound = -1003
    case emptyResponse = -1014
    case internalServerError = 500
    case cancelled = -999
    case parser = -13
    case invalidURL = -1122
}


/*
 NSError Extension to generate descriptive message
 */
extension Error {
    
    func generateNetworkError ()-> NSError {
        var errorKey: String = ""
        var errorDescription: String  = ""
        switch (self as NSError).code {
        case NetworkErrorCode.invalidURL.rawValue :
            errorKey = NetworkErrorConstants.Key.invalidURL
            errorDescription = NetworkErrorConstants.Description.invalidURL
            break
        case NetworkErrorCode.noConnectionDevice.rawValue :
            errorKey = NetworkErrorConstants.Key.noConnectionDevice
            errorDescription = NetworkErrorConstants.Description.noConnectionDevice
            break
        case NetworkErrorCode.noConnectionToHost.rawValue :
            errorKey = NetworkErrorConstants.Key.noConnectionToHost
            errorDescription = NetworkErrorConstants.Description.noConnectionToHost
            break
        case NetworkErrorCode.serverNotFound.rawValue :
            errorKey = NetworkErrorConstants.Key.serverNotFound
            errorDescription = NetworkErrorConstants.Description.serverNotFound
            break
        case NetworkErrorCode.connectionLost.rawValue :
            errorKey = NetworkErrorConstants.Key.connectionLost
            errorDescription = NetworkErrorConstants.Description.connectionLost
            break
        case NetworkErrorCode.timeOut.rawValue :
            errorKey = NetworkErrorConstants.Key.timeout
            errorDescription = NetworkErrorConstants.Description.timeOut
            break
        case NetworkErrorCode.emptyResponse.rawValue:
            errorKey = NetworkErrorConstants.Key.emptyResponse
            errorDescription = NetworkErrorConstants.Description.emptyResponse
            break
        case NetworkErrorCode.parser.rawValue :
            errorKey = NetworkErrorConstants.Key.parser
            errorDescription = NetworkErrorConstants.Description.parser
            break
        case NetworkErrorCode.cancelled.rawValue:
            errorKey = NetworkErrorConstants.Key.cancelled
            errorDescription = NetworkErrorConstants.Description.cancelled
            break
        default:
            return (self as NSError)
        }
        let userInfo: [String : Any] = [NSLocalizedDescriptionKey: errorDescription ,
                                        NSLocalizedFailureReasonErrorKey: errorKey ]
        
        let returnedError: NSError = NSError(domain: (self as NSError).domain, code: (self as NSError).code, userInfo: userInfo)
        
        return returnedError
    }
}
