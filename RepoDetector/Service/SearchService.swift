//
//  SearchService.swift
//  RepoDetector
//
//  Created by Ehab on 11/30/17.
//  Copyright © 2017 Github. All rights reserved.
//

import Foundation
/*
 SearchService is responsible for handling Search Service operations
 */
class SearchService : Service {
    var serviceRequest: Request
    var networkManager : NetworkManager
    var parserManger : ParserManager?
    var searchResult :  SearchResult = SearchResult()
    
    /*
     Intialization
     @param query type: String, like "Metal"
     @param page type: String, like "2"
     */
    init(query : String, page : String) {
        self.serviceRequest = SearchRequest(query: query, page: page)
        self.networkManager = NetworkManager(request: self.serviceRequest.request)
    }
    
    /*
     execute function
     @callback success with Any type or failure of NSError type
     */
    func execute(success:@escaping success, failure:  @escaping failure) {
        networkManager.execute(success: { (response) in
            if let responseData : Data = response {
                self.parserManger = ParserManager(data: responseData, model: self.searchResult, type: .json)
                self.parserManger?.parse(success: success, failure: failure)
            } else {
                success(nil)
            }
        }, failure: failure)
    }
}
