//
//  String+Localization.swift
//  RepoDetector
//
//  Created by Ehab on 11/30/17.
//  Copyright © 2017 Github. All rights reserved.
//

import Foundation
/*
 Extension for String is responsable for localization
 */
extension String {
    var localized: String {
        return NSLocalizedString(self, comment:"")
    }
}
