//
//  ParserManager.swift
//  RepoDetector
//
//  Created by Ehab on 11/29/17.
//  Copyright © 2017 Github. All rights reserved.
//

import Foundation

enum ParserType {
    case json, xml
}

/*
 Parser Manager is responsable for handing parsing Operations
 */
struct ParserManager {
    // Success callback with Any response
    typealias success = (Any?) -> Void
    
    // Failure callback with Any response
    typealias failure = (NSError) -> Void
    
    var data: Data
    var model: BaseModel
    var type: ParserType
    /*
     Initialization
     @param data type: Data
     @param model type: BaseModel
     @param type type: ParserType like .json
     */
    init(data: Data, model: BaseModel, type: ParserType) {
        self.data = data
        self.model = model
        self.type = type
    }
    
    /*
     parse function
     @callback success with Any type or failure of NSError type
     */
    func parse(success:@escaping success, failure:  @escaping failure){
        switch self.type {
        case .json:
            JSONObjectParser().parse(data: self.data, model: self.model, success: success, failure: failure)
            break
        case .xml:
            // not implemented, not required yet
            break
        }
    }
}
