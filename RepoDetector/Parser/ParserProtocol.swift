//
//  ParserProtocol.swift
//  RepoDetector
//
//  Created by Ehab on 11/29/17.
//  Copyright © 2017 Github. All rights reserved.
//

import Foundation
/*
 Base protocol of all Parsers handler
 */
protocol ParserProtocol {
    // Success callback with Any response
    typealias success = (Any?) -> Void
    
    // Failure callback with Any response
    typealias failure = (NSError) -> Void
    
    /*
     parse function
     @param data type: Data
     @param model type: BaseModel
     @callback success with Any type or failure of NSError type
     */
    func parse(data: Data, model: BaseModel, success:@escaping success, failure:  @escaping failure)
}
