//
//  SearchServiceTests.swift
//  RepoDetectorTests
//
//  Created by Ehab on 11/30/17.
//  Copyright © 2017 Github. All rights reserved.
//

import XCTest
@testable import RepoDetector

class SearchServiceTests: XCTestCase {
    var searchService : SearchService?
    override func setUp() {
        super.setUp()
        self.testInitialization()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInitialization() {
        let query : String = "Metal"
        let page : String = "1"
        self.searchService = SearchService(query: query, page: page)
        XCTAssertNotNil(searchService, "Search Service is nil")
        XCTAssertNotNil(searchService?.networkManager, "Network Manager is nil")
        XCTAssertNotNil(searchService?.networkManager.request, "Network Manager request is nil")
        if let searchRequest : SearchRequest = searchService?.serviceRequest as? SearchRequest {
            XCTAssertNotNil(searchService?.networkManager.request, "Network Manager request is nil")
            XCTAssertNotNil(searchService?.searchResult, "Search Service is nil")
            XCTAssertEqual(searchRequest.query, query)
            XCTAssertEqual(searchRequest.page, page)
        }
    }
    
    func testExecution(){
        let expectations = expectation(description: "test Exexution Request")
        self.searchService?.execute(success: { (response) in
            XCTAssertNotNil(response, "response is nil")
            expectations.fulfill()
        }, failure: { (error) in
            XCTAssertNotNil(error, "error is nil")
            XCTFail("Test Execution failed with Error: \(error)")
            expectations.fulfill()
        })
        waitForExpectations(timeout: 66) { (error) in
            if let error = error {
                XCTFail("testNormalExecute function execution time out error: \(error)")
            }
        }
    }
}
