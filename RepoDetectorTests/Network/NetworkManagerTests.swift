//
//  NetworkManagerTests.swift
//  RepoDetectorTests
//
//  Created by Ehab on 11/27/17.
//  Copyright © 2017 Github. All rights reserved.
//

import XCTest
import Mockingjay

@testable import RepoDetector

class NetworkManagerTests: XCTestCase {
    
    let urlString : String = "https://api.github.com/search/repositories?q=Metal&page=1&per_page=2"
    var networkManager : NetworkManager?
    
    override func setUp() {
        super.setUp()
        self.testInitialization()
    }
    
    func testInitialization() {
        // initialize url request
        if let encodedURL : String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed),
            let url : URL = URL(string: encodedURL) {
            let urlRequest : URLRequest = URLRequest.init(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 10)
            XCTAssertNotNil(urlRequest, "URL Request is nil")
            // inializae NetworkManager with request
            networkManager = NetworkManager(request: urlRequest)
            XCTAssertNotNil(networkManager, "Network Manager is nil")
            XCTAssertNotNil(networkManager?.request, "Network Manager request variable is nil")
        } else {
            XCTFail("initialize is failed becasue of encoded URL")
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // Test Normal time Request
    func testNormalExecute(){
        // make sure networkManager is not nil
        XCTAssertNotNil(networkManager, "networkManager is nil")
        unowned let expectations = expectation(description: "test Exexution Request")
        networkManager?.execute(success: { (response) in
            XCTAssertNotNil(response, "response is nil")
            expectations.fulfill()
        }, failure: { (error) in
            XCTAssertNotNil(error, "error is nil")
            XCTAssertNotNil(error.code, "error code is empty")
            XCTFail("Execution is failed with error: \(error)")
            expectations.fulfill()
        })
        waitForExpectations(timeout: 5) { (error) in
            if let error = error {
                XCTFail("testNormalExecute function execution time out error: \(error)")
            }
        }
    }
    
    func testSlowTimeExecute() {
        // check if networkManager is not equal nil
        XCTAssertNotNil(networkManager, "networkManager is nil")
        let timeOut : TimeInterval = 3
        networkManager?.request.timeoutInterval = timeOut
        XCTAssertNotNil(networkManager?.request, "Network Manager request variable is nil")
        unowned let expectations = expectation(description: "test Slow time Execution Request")
        networkManager?.execute(success: { (response) in
            XCTAssertNotNil(response, "response is nil")
            expectations.fulfill()
        }, failure: { (error) in
            XCTAssertNotNil(error, "error is nil")
            XCTAssertNotNil(error.code, "error code is empty")
            XCTFail("Execution is failed with error: \(error)")
            expectations.fulfill()
        })
        waitForExpectations(timeout: timeOut) { (error) in
            if let error = error {
                XCTFail("testNormalExecute function execution time out error: \(error)")
            }
        }
    }
    
    func testcancelAllTasks() {
        XCTAssertNotNil(networkManager, "networkManager is nil")
        unowned let expectations = expectation(description: "test Exexution Request")
        networkManager?.execute(success: { (response) in
            expectations.fulfill()
        }, failure: { (error) in
            XCTAssertEqual(error.code, NetworkErrorCode.cancelled.rawValue)
            expectations.fulfill()
        })
        networkManager?.cancelAllTasks()
        waitForExpectations(timeout: 5) { (error) in
            if let error = error {
                XCTFail("testNormalExecute function execution time out error: \(error)")
            }
        }
    }
    
    func testGetMetalGithubRepo(){
        
        if let path = Bundle(for: type(of: self)).path(forResource: "MetalSearchSuccess", ofType: "json"),let data = NSData(contentsOfFile: path),  let encodedURL : String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
            self.stub(uri(encodedURL), jsonData(data as Data))
            XCTAssertNotNil(networkManager, "networkManager is nil")
            
            unowned let expectations = expectation(description: "test Exexution Request")
            
            networkManager?.execute(success: { (responseData) in
                XCTAssertNotNil(responseData, "response data is nil")
                let parserManger = ParserManager(data: responseData!, model: SearchResult(), type: .json)
                parserManger.parse(success: { (response) in
                    XCTAssertNotNil(response, "response is nil")
                    expectations.fulfill()
                }, failure: { (error) in
                    XCTFail("parserManger parse function execution time out error: \(error)")
                    expectations.fulfill()
                })
            }, failure: { (error) in
                XCTAssertEqual(error.code, NetworkErrorCode.cancelled.rawValue)
                expectations.fulfill()
            })
            waitForExpectations(timeout: 5) { (error) in
                if let error = error {
                    XCTFail("testNormalExecute function execution time out error: \(error)")
                }
            }
        }else{
            XCTFail("Failed to read file")
        }
    }
}
