//
//  StateManagerTests.swift
//  RepoDetectorTests
//
//  Created by Ehab on 11/27/17.
//  Copyright © 2017 Github. All rights reserved.
//

import XCTest
@testable import RepoDetector

class StateManagerTests: XCTestCase {
    
    var stateManager: StateManager = StateManager()
    
    override func setUp() {
        super.setUp()
        self.testSetEnvironment()
    }
    
    func testCalacualtedServiceConfigration() {
        let serviceEnvironment : ServiceEnvironment = .production
        XCTAssertNotNil(serviceEnvironment, "Service Environment is nil")
        XCTAssertNotNil(serviceEnvironment.rawValue, "production")
        XCTAssertEqual(serviceEnvironment.serviceConfigration.baseURL, ProductionConfigrations.baseURL.rawValue)
        XCTAssertEqual(serviceEnvironment.serviceConfigration.baseURL, ProductionConfigrations.baseURL.rawValue)
    }
    
    func testSetEnvironment() {
        XCTAssertNotNil(stateManager, "State Manager is nil")
        stateManager.set(environment: .production)
        XCTAssertEqual(stateManager.serviceConfigration.baseURL, ServiceEnvironment.production.serviceConfigration.baseURL)
        let savedAppServiceEnvironmentKey : String = "AppServiceEnvironment"
        XCTAssertEqual("AppServiceEnvironment",  savedAppServiceEnvironmentKey)
        if let appServiceEnvironment : String = UserDefaults.standard.value(forKey: savedAppServiceEnvironmentKey) as? String{
            XCTAssertEqual(appServiceEnvironment, ServiceEnvironment.production.rawValue)
        } else{
            XCTFail("App Service Environment is empty or nil vlaue or key is worng")
        }
    }
    
    func testGetCurrentEnvironment(){
        XCTAssertNotNil(stateManager, "State Manager is nil")
        if let currentEnvironment : ServiceEnvironment = self.stateManager.getCurrentEnvironment(){
            XCTAssertEqual(currentEnvironment,  .production)
        } else{
            XCTFail("App Service Environment is empty or nil vlaue or key is worng")
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
}
