//
//  ServiceConfigrationTests.swift
//  RepoDetectorTests
//
//  Created by Ehab on 11/27/17.
//  Copyright © 2017 Github. All rights reserved.
//

import XCTest
@testable import RepoDetector

class ServiceConfigrationTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        self.testInitialization()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    func testInitialization() {
        let baseURL : String = "www.yahoo.com"
        let serviceConfigration : ServiceConfigration =  ServiceConfigration(baseURL: baseURL)
        XCTAssertNotNil(serviceConfigration, "Service Configration is nil")
        XCTAssertEqual(serviceConfigration.baseURL, baseURL)
    }
    func testCalculatedProductionConfigrations(){
        let baseURL: String = "https://api.github.com"
        XCTAssertEqual(ProductionConfigrations.baseURL.rawValue, baseURL)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
}
