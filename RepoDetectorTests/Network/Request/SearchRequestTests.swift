//
//  SearchRequestTests.swift
//  RepoDetectorTests
//
//  Created by Ehab on 11/27/17.
//  Copyright © 2017 Github. All rights reserved.
//

import XCTest
@testable import RepoDetector

class SearchRequestTests: XCTestCase {
    var searchRequest : SearchRequest?
    let urlString : String = "https://api.github.com/search/repositories?q=Metal&page=3&per_page=50"
    
    override func setUp() {
        super.setUp()
        ServiceConfigrationTests().testCalculatedProductionConfigrations()
        searchRequest = SearchRequest(query: "Metal", page: "3")
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    func testSearchRequestParamters() {
        let pathKey: String = "/search/repositories?"
        let queryKey: String = "q="
        let pageKey: String = "&page="
        let perPageKey: String = "&per_page="
        XCTAssertEqual(SearchRequestParamters.pathKey.rawValue, pathKey)
        XCTAssertEqual(SearchRequestParamters.queryKey.rawValue, queryKey)
        XCTAssertEqual(SearchRequestParamters.pageKey.rawValue, pageKey)
        XCTAssertEqual(SearchRequestParamters.perPageKey.rawValue, perPageKey)
    }
    
    func testInitialization(){
        XCTAssertNotNil(searchRequest, "Search Request is nil")
        XCTAssertEqual(searchRequest?.query, "Metal")
        XCTAssertEqual(searchRequest?.page, "3")
        XCTAssertEqual(searchRequest?.perPage, "50")
        // test request for production
        if let request : URLRequest = searchRequest?.request {
            XCTAssertNotNil(request, "Search Request is nil")
            XCTAssertNotNil(request.url, "Search Request url is nil")
            XCTAssertNotNil(request.httpMethod, "Search Request http Method is nil")
            XCTAssertNotNil(request.timeoutInterval, "Search Request timeout Interval is nil")
            XCTAssertNotNil(request.cachePolicy, "Search Request cache Policy is nil")
            if let encodedURL : String = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
                XCTAssertEqual(request.url?.absoluteString, encodedURL)
            } else{
                XCTFail("Encoding URL is failed")
            }
        } else{
            XCTFail("request is nil")
        }
    }
    
    func testGetSearchURLRequestPath() {
        
        XCTAssertNotNil(searchRequest, "Search Request is nil")
        let serviceConfigration : ServiceConfigration = StateManager().serviceConfigration
        let apiPath : String = serviceConfigration.baseURL +  SearchRequestParamters.pathKey.rawValue
        let queryPath : String = SearchRequestParamters.queryKey.rawValue + (searchRequest?.query ?? "")
        let pagePath : String = SearchRequestParamters.pageKey.rawValue + (searchRequest?.page ?? "")
        let perPagePath : String = SearchRequestParamters.perPageKey.rawValue + (searchRequest?.perPage ?? "")

        let path : String = apiPath + queryPath + pagePath + perPagePath

        let searchURLRequestPath : String = (searchRequest?.getURLPath(serviceConfigration: serviceConfigration))!
        XCTAssertEqual(path, searchURLRequestPath)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
}
